<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller{
  public function __construct(){
    parent::__construct();
    //LOAD ALL REQUIRED MODEL 
    $this->load->model('login_model');
  }    
    //index
    public function index(){ 
        $data['controller_name']  = 'login';
        $data['view']             = 'frontend/login';
        $data['page_name']        = 'login';
        $this->load->view("layouts/login",$data);
    }
    //logout
    public function logout(){
        $this->session->sess_destroy();   
        redirect('login');
    }
    //check_user
    public function check_user(){
        $username   = $this->input->post('username');
        $password   = $this->input->post('password');
        $status     = $this->login_model->check_user($username, $password);
        if($status == 1){
        $user_data    = $this->login_model->get_user_data($username);
        $user_id      = $user_data->user_id;
        $user_role    = $user_data->user_role;
        $first_name   = $user_data->first_name;
        $last_name    = $user_data->last_name;
        $user_details = $this->login_model->get_session_data($user_id, $user_role, $first_name, $last_name);
            $session_data = array(
                'logged_in'     => TRUE, 
                'username'      => $username,
                'user_role'     => $user_role, 
                'user_id'       => $user_id,
                'first_name'    => $first_name, 
                'last_name'     => $last_name, 
                'profile_photo' => $user_details->image,
                'aadhar_no'     => $aadhar_no,
                'email'         => $user_details->email,
                'mobile_no'     => $user_details->mobile_no,
            );  
            $this->session->set_userdata($session_data);
            if($user_role == 'admin'){
                redirect('admin');
            }
            if ($user_role == 'staff'){
                redirect('staff_template');
            }else{
                $this->session->set_flashdata('msg','Either username or password is incorrect, Please try again.');
                redirect('login');
            }   
        }else{
                $this->session->set_flashdata('msg','Either username or password is incorrect, Please try again.');
                redirect('login');
            }
    }
    //forget_pass
    public function forget_pass(){
        $data['controller_name']  =  'login';
        $data['view']             = 'frontend/forget_pass';
        $data['page_name']        = 'forget_pass';
        $this->load->view("jp_insurance/frontend/forget_pass",$data);
    }
    //change_password
    public function change_password(){
        $result=$this->login_model->change_password();
        $this->session->set_flashdata('success_message', $result);
        redirect("login");
    }
}
?>