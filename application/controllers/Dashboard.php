<?php

class Dashboard extends CI_Controller {
  public function __construct()
  {
     parent::__construct();
       $this->load->database();
         $this->load->model('ListClientModel');
  }
	public function index()
	{
		
         $data['h']=$this->ListClientModel->getClient();  
         //return the data in view  
         $this->load->view('clientlist', $data);  
	}
	
}
